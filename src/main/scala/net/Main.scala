package net

import zio._
import zio.logging.backend.SLF4J

object Main extends ZIOAppDefault {
  def run: ZIO[Environment with ZIOAppArgs with Scope, Any, Any] =
    ZIO.logInfo("asdasdf").provideLayer(Runtime.removeDefaultLoggers >>> SLF4J.slf4j)
}