scalaVersion := "2.13.10"

libraryDependencies ++= Seq(
  "dev.zio" %% "zio" % "2.0.9",
  "dev.zio" %% "zio-logging-slf4j"  % "2.1.10",
  "ch.qos.logback" % "logback-classic" % "1.4.5",
  "net.logstash.logback" % "logstash-logback-encoder" % "7.2",
)